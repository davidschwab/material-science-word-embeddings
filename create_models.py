import time
import json
import binascii
import hashlib
import os
import argparse
from typing import Union
from pathlib import Path

import gensim.utils
import nltk
import numpy as np
import gensim.downloader as api
from gensim.models import Word2Vec, KeyedVectors
from gensim.test.utils import datapath
from nltk.corpus import wordnet
from nltk.corpus import stopwords

import examples
from project_config import WORDS_ANALOGY_EVALUATION_SET, PRETRAINED_MODEL_PATH, \
    FROM_SCRATCH_MODEL_PATH, EVALUATIONS_PATH, CORPUS_PATH, ELEMENTS_ANALOGY_EVALUATION_SET
from w2v_utils import get_similar_to_relation

# Uncomment to enable logging for gensim
# logging.basicConfig(format="%(levelname)s - %(asctime)s: %(message)s", datefmt= '%H:%M:%S', level=logging.INFO)
nltk.download('wordnet')
nltk.download('stopwords')
nltk.download('omw-1.4')

EPOCHS = 300
EMBEDDING_DIM = 300
WINDOWS_SIZE = 2
MIN_COUNT = 2  # minimum occurrence of word

WORKERS = 16

EVAL_TOP_N = 10

CORPUS_PATH_ARG = 'corpus_path'
TRAIN_FROM_SCRATCH_ARG = 'train_from_scratch'
FINETUNE_ARG = 'finetune'
CONVERTED_PRETRAINED_ARG = 'convert_pretrained'
HYPERPARAMETR_GRID_ARG = 'hyperparameter_grid'

lemmatizer = nltk.wordnet.WordNetLemmatizer()

STOPWORDS = stopwords.words('english') + ['is']


def generate_corpus(corpus_folder: Path):
    corpus = []

    for file in corpus_folder.iterdir():
        if file.name.endswith("txt"):
            text = open(os.path.join(file), 'r').read()
            corpus.append(list(gensim.utils.tokenize(text, lower=True)))

    return corpus


# https://radimrehurek.com/gensim/models/word2vec.html#gensim.models.word2vec.LineSentence
# https://www.kaggle.com/code/pierremegret/gensim-word2vec-tutorial/notebook
def train_w2v(corpus, from_scratch_model_path=FROM_SCRATCH_MODEL_PATH):
    w2v_model = Word2Vec(vector_size=EMBEDDING_DIM, window=WINDOWS_SIZE,
                         min_count=MIN_COUNT,
                         workers=WORKERS)

    w2v_model.build_vocab(corpus, progress_per=10)
    w2v_model.train(corpus, total_examples=w2v_model.corpus_count, epochs=EPOCHS, report_delay=1)
    w2v_model.save(str(from_scratch_model_path))
    return w2v_model


def finetune_w2v(corpus, w2v_model: Word2Vec = FROM_SCRATCH_MODEL_PATH,
                 pretrained_model_path: Path = PRETRAINED_MODEL_PATH, train_model: bool = False):
    if train_model:
        w2v_model = Word2Vec(vector_size=EMBEDDING_DIM, window=WINDOWS_SIZE,
                             min_count=MIN_COUNT,
                             workers=WORKERS)
        w2v_model.build_vocab(corpus)
    else:
        w2v_model = Word2Vec.load(str(w2v_model))
    w2v_model.wv.vectors_lockf = np.ones(len(w2v_model.wv))
    w2v_model.wv.intersect_word2vec_format(pretrained_model_path, binary=True)
    if train_model:
        w2v_model.train(corpus, total_examples=w2v_model.corpus_count, epochs=EPOCHS)
    w2v_model.save(str(pretrained_model_path)[:-4] + "_finetuned.model")
    return w2v_model


# .bin needs to be loaded to w2v format
def convert_pretrained_w2v(pretrained_model_path: Path = PRETRAINED_MODEL_PATH):
    download_pretrained_model(pretrained_model_path)
    pretrained_model = KeyedVectors.load_word2vec_format(str(pretrained_model_path), binary=True)
    pretrained_model.save(str(pretrained_model_path)[:-4] + ".model")
    return pretrained_model


def download_pretrained_model(pretrained_model_path):
    if not pretrained_model_path.exists():
        download_path = api.load('word2vec-google-news-300', return_path=True)
        os.replace(download_path, pretrained_model_path)


def calculate_checksum(path: Path):
    hash_value = hashlib.md5()
    for file in path.iterdir():
        if file.is_file() and file.name.endswith('.txt'):
            hash_value.update(open(file, "rb").read())
    return int(binascii.hexlify(hash_value.digest()), 16)


def get_wordnet_pos(word):
    """Map POS tag to first character lemmatize() accepts"""
    tag = nltk.pos_tag([word])[0][1][0].upper()
    tag_dict = {"J": wordnet.ADJ,
                "N": wordnet.NOUN,
                "V": wordnet.VERB,
                "R": wordnet.ADV}

    return tag_dict.get(tag, wordnet.NOUN)


def lemmatize_word(word: str) -> str:
    return lemmatizer.lemmatize(word, get_wordnet_pos(word))


def post_process(results: [str, int], filtered_words=None) -> [str]:
    if filtered_words is None:
        filtered_words = []

    word_results = [a[0] for a in results]
    return list(set(lemmatize_word(a) for a in word_results if
                    a not in filtered_words and a not in STOPWORDS))


def most_similar(element, wv):
    try:
        return post_process(
            wv.most_similar(element, EVAL_TOP_N), filtered_words=[element])
    except KeyError:
        return []


def evaluate_model(w2vModel_or_wv: Union[Word2Vec, KeyedVectors], train_strategy: str,
                   corpus_path: Path):
    if isinstance(w2vModel_or_wv, Word2Vec):
        wv = w2vModel_or_wv.wv
    else:
        wv = w2vModel_or_wv

    word_analogy_scores = wv.evaluate_word_analogies(
        datapath(str(WORDS_ANALOGY_EVALUATION_SET)))
    element_analogy_scores = wv.evaluate_word_analogies(
        str(ELEMENTS_ANALOGY_EVALUATION_SET))

    timestamp = time.strftime("%d.%m.%Y_%H:%M:%S")

    hyperparameter_dict = None

    if isinstance(w2vModel_or_wv, Word2Vec):
        w2vModel = w2vModel_or_wv
        hyperparameter_dict = {'vector_size': w2vModel.vector_size,
                               'epochs': w2vModel.epochs,
                               'min_count': w2vModel.min_count,
                               'window': w2vModel.window,
                               'algorithm': 'skip-gramm' if w2vModel.sg == 1 else 'cbow',
                               'alpha': w2vModel.alpha,
                               'cbow_mean': w2vModel.cbow_mean,
                               }

    evaluation_dicts = {'train_strategy': train_strategy,
                        'time':
                            timestamp,
                        'hyperparameters': hyperparameter_dict,
                        'metrics': {
                            'questions-words-analogy_scores': word_analogy_scores[0],
                            'questions-elements-analogy_scores': element_analogy_scores[0]
                        },
                        'similarity': {
                            element: most_similar(element, wv) for
                            element in examples.element_list_lowercase[:EVAL_TOP_N]
                        },

                        'relations': {
                            str((relation[0], relation[1], relation[2])): post_process(
                                get_similar_to_relation(
                                    wv, relation[0], relation[1], relation[2], EVAL_TOP_N),
                                filtered_words=[relation[0], relation[1], relation[2]])
                            for relation in examples.relations_elements
                        },
                        'corpus': {
                            'md5Hash': calculate_checksum(corpus_path)
                        }
                        }

    json_str = json.dumps(evaluation_dicts, indent=4)

    with open(f'{str(EVALUATIONS_PATH)}/{train_strategy}_{timestamp}.json', 'w') as f:
        f.write(json_str)


def train_and_eval_from_scratch():
    if config[TRAIN_FROM_SCRATCH_ARG] is None:
        from_scratch_model = train_w2v(corpus)
    else:
        from_scratch_model = train_w2v(corpus, Path(config[TRAIN_FROM_SCRATCH_ARG]))
    evaluate_model(from_scratch_model, TRAIN_FROM_SCRATCH_ARG, CORPUS_PATH)


def train_and_eval_finetuned_model(train_model: bool):
    if config[FINETUNE_ARG] is None:
        finetuned_model = finetune_w2v(corpus, train_model=train_model)
    else:
        finetuned_model = finetune_w2v(corpus, config[FINETUNE_ARG][0], config[FINETUNE_ARG][1],
                                       config[FINETUNE_ARG][2])
    evaluate_model(finetuned_model, FINETUNE_ARG, CORPUS_PATH)


def convert_and_eval_pretrained():
    if config[CONVERTED_PRETRAINED_ARG] is None:
        pretrained_model = convert_pretrained_w2v()
    else:
        pretrained_model = convert_pretrained_w2v(Path(config[CONVERTED_PRETRAINED_ARG]))

    evaluate_model(pretrained_model, CONVERTED_PRETRAINED_ARG, CORPUS_PATH)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Train and save models in trainedModels dir.",
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-fs", f"--{TRAIN_FROM_SCRATCH_ARG}", nargs='?', default=argparse.SUPPRESS,
                        help="train_model Word2Vec model from scratch")
    parser.add_argument("-ft", f"--{FINETUNE_ARG}", nargs='?', default=argparse.SUPPRESS,
                        help="finetune Word2Vec model, 1: w2v model to finetune, 2: pretrained "
                             "model (default: " + str(
                            [FROM_SCRATCH_MODEL_PATH, PRETRAINED_MODEL_PATH]) + ")")
    parser.add_argument("-cp", f"--{CONVERTED_PRETRAINED_ARG}", nargs='?',
                        default=argparse.SUPPRESS,
                        help="convert pretrained .bin to .model (default: " + str(
                            PRETRAINED_MODEL_PATH) + ")")
    parser.add_argument("-hp", f"--{HYPERPARAMETR_GRID_ARG}", nargs='?',
                        default=argparse.SUPPRESS,
                        help='start hyperparameter grid for from scratch and fine-tuned model')

    parser.add_argument("-c", f"--{CORPUS_PATH_ARG}", nargs=1, default=argparse.SUPPRESS,
                        help=f"corpus to train_model on (default: {CORPUS_PATH}")
    args = parser.parse_args()
    config = vars(args)

    if CORPUS_PATH_ARG in args:
        corpus = config[CORPUS_PATH_ARG]
    else:
        corpus = generate_corpus(CORPUS_PATH)

    if HYPERPARAMETR_GRID_ARG in args:

        for epoch in [5, 10, 100, 200, 300]:
            EPOCHS = epoch
            for min_count in [2, 3, 4, 5, 6, 7]:
                MIN_COUNT = min_count
                for window in [2, 4, 8, 12]:
                    WINDOWS_SIZE = window
                    finetuned_model = finetune_w2v(corpus, train_model=True)
                    evaluate_model(finetuned_model, FINETUNE_ARG, CORPUS_PATH)
    else:

        if TRAIN_FROM_SCRATCH_ARG in args:
            train_and_eval_from_scratch()

        if FINETUNE_ARG in args:
            train_and_eval_finetuned_model(train_model=True)

        if CONVERTED_PRETRAINED_ARG in args:
            convert_and_eval_pretrained()
