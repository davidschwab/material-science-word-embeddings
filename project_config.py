from pathlib import Path

SAVED_MODEL_PATH = Path('trainedModels')
SAVED_MODEL_PATH.mkdir(exist_ok=True)

CORPUS_PATH = Path('corpus')
CORPUS_PATH.mkdir(exist_ok=True)

EVALUATIONS_PATH = Path('evaluations')
EVALUATIONS_PATH.mkdir(exist_ok=True)

REDUCED_MODEL_PATH = Path("reducedModels")
SAVED_MODEL_PATH.mkdir(exist_ok=True)

PRETRAINED_MODEL_PATH = Path('trainedModels/GoogleNews-vectors-negative300.bin')
FROM_SCRATCH_MODEL_PATH = Path("trainedModels/w2v_from_scratch.model")
WORDS_ANALOGY_EVALUATION_SET = Path('questions-words.txt')
ELEMENTS_ANALOGY_EVALUATION_SET = Path('questions-elemets.txt')
