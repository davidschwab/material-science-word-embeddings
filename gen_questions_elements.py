def get_none_n_times(n):
    result = []
    for _ in range(n):
        result.append(None)

    return result


PERIODIC_TABLE = [['Hydrogen'] + get_none_n_times(16) + ['Helium'],

                  ['Lithium',
                   'Beryllium'] +
                  get_none_n_times(10) +
                  ['Boron',
                   'Carbon',
                   'Nitrogen',
                   'Oxygen',
                   'Fluorine',
                   'Neon'],

                  ['Sodium',
                   'Magnesium'] +
                  get_none_n_times(10) +
                  ['Aluminium',
                   'Silicon',
                   'Phosphorus',
                   'Sulfur',
                   'Chlorine',
                   'Argon'],

                  ['Potassium',
                   'Calcium',
                   'Scandium',
                   'Titanium',
                   'Vanadium',
                   'Chromium',
                   'Manganese',
                   'Iron',
                   'Cobalt',
                   'Nickel',
                   'Copper',
                   'Zinc',
                   'Gallium',
                   'Germanium',
                   'Arsenic',
                   'Selenium',
                   'Bromine',
                   'Krypton'],

                  ['Rubidium',
                   'Strontium',
                   'Yttrium',
                   'Zirconium',
                   'Niobium',
                   'Molybdenum',
                   'Technetium',
                   'Ruthenium',
                   'Rhodium',
                   'Palladium',
                   'Silver',
                   'Cadmium',
                   'Indium',
                   'Tin',
                   'Antimony',
                   'Tellurium',
                   'Iodine',
                   'Xenon'],

                  ['Caesium',
                   'Barium',
                   None,
                   'Hafnium',
                   'Tantalum',
                   'Tungsten',
                   'Rhenium',
                   'Osmium',
                   'Iridium',
                   'Platinum',
                   'Gold',
                   'Mercury',
                   'Thallium',
                   'Lead',
                   'Bismuth',
                   'Polonium',
                   'Astatine',
                   'Radon'],

                  ['Francium',
                   'Radium',
                   None,
                   'Rutherfordium',
                   'Dubnium',
                   'Seaborgium',
                   'Bohrium',
                   'Hassium',
                   'Meitnerium',
                   'Darmstadtium',
                   'Roentgenium',
                   'Copernicium',
                   'Nihonium',
                   'Flerovium',
                   'Moscovium',
                   'Livermorium',
                   'Tenness',
                   'Oganesson']
                  ]

def is_main_group(j):
    return j < 2 or j > 11


if __name__ == '__main__':

    relations = []

    for i in range(len(PERIODIC_TABLE) - 1):
        for j in range(len(PERIODIC_TABLE[i])):
            if is_main_group(j):
                rel_1 = PERIODIC_TABLE[i][j]
                rel_2 = PERIODIC_TABLE[i + 1][j]

                for j_2 in range(len(PERIODIC_TABLE[i])):
                    if j != j_2 and is_main_group(j_2):
                        try:
                            rel_3 = PERIODIC_TABLE[i][j_2]
                            rel_4 = PERIODIC_TABLE[i + 1][j_2]
                        except Exception as e:
                            print(e)

                        if rel_1 is not None and rel_2 is not None and rel_3 is not None and rel_4 is not None:
                            relations.append((rel_1, rel_2, rel_3, rel_4))

    output_string = ": elements"
    for relation_tuple in relations:
        output_string += '\n'
        output_string += f"{relation_tuple[0]} {relation_tuple[1]} {relation_tuple[2]} {relation_tuple[3]}"

    with open("questions-elemets.txt", "w+") as file:
        file.write(output_string)
