from fastapi import Response, APIRouter

from model_selection import ModelSelector
from requestModels import DisplayVectorsRequest, RelationRequest
from w2v_utils import get_similar_to_relation

model_selector = ModelSelector()
api = APIRouter(prefix="/api")


@api.get('/tokens')
def tokens(model: str):
    w2v_model = model_selector.get_model_from_name(model)
    all_tokens = list(w2v_model.wv.key_to_index.keys())
    all_tokens.sort()
    return all_tokens


@api.get("/similarity")
def top_n_similar(word: str, model: str, response: Response, n: int = 10):
    try:
        w2v_model = model_selector.get_model_from_name(model)
        return json_similarity_response(word, n, w2v_model)
    except KeyError:
        response.status_code = 404
        return 'Word not found'


def json_similarity_response(word, n, w2v_model):
    result = []
    for idx, word in enumerate(w2v_model.wv.most_similar(word, topn=n)):
        result.append({'rank': idx, 'word': word[0], 'similarity': word[1]})
    return result


@api.get("/models")
def models(resp: Response):
    resp.media_type = 'application/json'
    return model_selector.model_names


@api.get("/distances")
def distances(word_1: str, word_2: str, model: str, response: Response):
    try:
        w2v_model = model_selector.get_model_from_name(model)
        return w2v_model.wv.distances(word_1, [word_2]).tolist()
    except KeyError:
        response.status_code = 404
        return 'Word not found'


@api.post("/display_vectors")
def display_vectors(data: DisplayVectorsRequest):
    reduced_model = model_selector.get_vectors_from_label(data.vectors, data.model)
    return reduced_model.__dict__


@api.get("/check_word_occurrence")
def check_word_occurrence(word: str, model: str):
    return model_selector.is_word_in_model(word, model)


@api.post("/relation")
def relation(data: RelationRequest, response: Response):
    try:
        w2v_model = model_selector.get_model_from_name(data.model)
        similar_by_vector = get_similar_to_relation(w2v_model.wv, data.first, data.second, data.third, data.top_n)
        return [{'rank': idx, 'word': word[0], 'similarity': word[1]} for idx, word in enumerate(similar_by_vector)]
    except KeyError:
        response.status_code = 404
        return 'Word not found'

