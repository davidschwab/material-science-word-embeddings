from model_selection import ModelSelector
from w2v_utils import get_similar_to_relation
import json
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import pdfkit


model_selector = ModelSelector()

json_file_path = "frontend/public/co_occurences.json"

with open(json_file_path, 'r') as j:
     co_occurences = json.loads(j.read())

print(co_occurences.keys())
print(co_occurences['hydrogen'])

first_element = 'acetate'
snd_element = 'hydrogen'
trd_element = 'crystal'
top_n = 10
model = 'GoogleNews-vectors-negative300_finetuned'


def relation(model, first_element, snd_element, trd_element, top_n):
    try:
        w2v_model = model_selector.get_model_from_name(model)
        similar_by_vector = get_similar_to_relation(w2v_model.wv, first_element, snd_element, trd_element, top_n)
        return [{'rank': idx, 'word': word[0], 'similarity': word[1]} for idx, word in enumerate(similar_by_vector)]
    except KeyError:
        return 'Word not found'


def all_relations(model, top_n):
    try:
        results = {}
        all_similarities = []
        w2v_model = model_selector.get_model_from_name(model)
        combinations = []
        for k in co_occurences.keys():
            first_element = k
            all_per_first = []
            for element in co_occurences[k]:
                all_per_second = []
                snd_element = list(element.keys())[0]
                for trd_element in co_occurences[list(element.keys())[0]]:
                    try:
                        trd_element = list(trd_element.keys())[0]
                        similar_by_vector = get_similar_to_relation(w2v_model.wv, first_element, snd_element,
                                                                    trd_element, top_n)
                        top_similar_elements = [{'rank': idx, 'word': word[0], 'similarity': word[1]} for idx, word in
                                                 enumerate(similar_by_vector)]
                        combinations.append([first_element, snd_element, trd_element, top_similar_elements[0]])
                        all_similarities.append(top_similar_elements)
                    except:
                        continue
                all_per_first.append(all_per_second)
            results[k] = all_per_first

        return results, combinations, all_similarities
    except KeyError:
        return 'Word not found'


def extract(lst):
    return [item[0] for item in lst]


results, combinations, all_similarities = all_relations(model, top_n)
df = pd.DataFrame(combinations, columns = ['1st_element', '2nd_element', '3rd_element', 'top_similar_element'])
df = df.copy()
df['top_10_similar_elements'] = all_similarities


df = df[df['1st_element'] != '319']
df = df[df['2nd_element'] != '319']
df = df[df['3rd_element'] != '319']
df = df[df['1st_element'] != 'basic']
df = df[df['2nd_element'] != 'basic']
df = df[df['3rd_element'] != 'basic']
df = df[df['1st_element'] != 'partially']
df = df[df['2nd_element'] != 'partially']
df = df[df['3rd_element'] != 'partially']
df = df[df['1st_element'] != 'of']
df = df[df['2nd_element'] != 'of']
df = df[df['3rd_element'] != 'of']
df = df[df['1st_element'] != 'reagent']
df = df[df['2nd_element'] != 'reagent']
df = df[df['3rd_element'] != 'reagent']

df = df.sort_values(by="top_similar_element", key=lambda k: k.str["similarity"], ascending=False)
df.to_csv("./new_relations_topdf.csv", sep='\t')

df = df.drop('top_10_similar_elements', axis=1)
"""
html_string = df.to_html()
pdfkit.from_string(html_string, "output_file.pdf")
print("PDF file saved.")

"""

#https://stackoverflow.com/questions/32137396/how-do-i-plot-only-a-table-in-matplotlib
fig, ax =plt.subplots(figsize=(12,4))
ax.axis('tight')
ax.axis('off')
the_table = ax.table(cellText=df.head(500).values,colLabels=df.head(500).columns,loc='center')

#https://stackoverflow.com/questions/4042192/reduce-left-and-right-margins-in-matplotlib-plot
pp = PdfPages("foo.pdf")
pp.savefig(fig, bbox_inches='tight')
pp.close()

"""

print(all_relations(model, top_n))
with open('relations_to_pdf.json', 'w') as f:
    json.dump(all_relations(model, top_n), f)
result = json.dumps(all_relations(model, top_n))
"""