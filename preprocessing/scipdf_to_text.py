# https://github.com/titipata/scipdf_parser
import os
import shutil

import scipdf
import string
from pathlib import Path
import re
from tqdm import tqdm
import nltk

from nltk.corpus import stopwords
from nltk.corpus import wordnet

import examples

nltk.download('stopwords')
nltk.download('averaged_perceptron_tagger')

"""
artifact: bc I was too lazy to use bash or copy-paste
def move_files():
    for folder in os.listdir("../corpus/LCaron-ArticleLibrary/"):
        for folder_ in os.listdir("../corpus/LCaron-ArticleLibrary/BibDesk/"):
            for filename in os.listdir("../corpus/LCaron-ArticleLibrary/BibDesk/" + str(folder_) + "/"):
                if filename.endswith(".pdf"):
                    shutil.copyfile("../corpus/LCaron-ArticleLibrary/BibDesk/" + str(folder_) + "/" + str(filename), "../corpus/" + str(filename))
"""


class Document:
    text = []
    output_file_type: str = ".txt"
    output_file_path: str

    def __init__(self, file_path: str):
        print(file_path)
        self.all = scipdf.parse_pdf_to_dict(file_path)  # return dictionary
        self.text = []
        self.text.append(self.all['abstract'])

        for section in range(len(self.all['sections'])):
            self.text.append(self.all['sections'][section]['text'])
        #print(len(self.text))
        self.text = ' '.join(self.text)
        #print(len(self.text))
        self.output_file_path = file_path[:-4]

    def remove_whitespace_before_and_after_numbers(self):

        # replacement function to convert uppercase letter to lowercase
        def remove_whitespace(match_obj):
            if match_obj.group() is not None:
                return match_obj.group().replace(" ", "")

        self.text = re.sub("(?:[A-Z]+)\s+(?:\d+(?:(?:[\.|\,])\d+(?:\%)?)?)\s+(?:[A-Z]+)\s+(?:\d+(?:(?:[\.|\,])\d+(?:\%)?)?)*\s*(?:[A-Z]*)\s*(?:\d+(?:(?:[\.|\,])\d+(?:\%)?)?)*", remove_whitespace, self.text)


    def save_text(self):
        with open(self.output_file_path + self.output_file_type, 'w') as file:
            file.write(self.text)

    def remove_line_break_dash(self):
        self.text = self.text.replace('-\n', '')

    def remove_extra_white_spaces(self):
        self.text = self.text.strip()
        self.text = re.sub(" +", " ", self.text)

    def replace_format_chars(self, replacement=" "):
        self.text = re.sub("[\t\n\x0b\r\f]", replacement, self.text)

    def remove_non_ascii(self):
        printable = set(string.printable)
        self.text = ''.join(filter(lambda x: x in printable, self.text))

    def remove_punctuation(self):
        self.text = self.text.translate(str.maketrans('', '', string.punctuation))
        self.text = ''.join(self.text)

    def tokenize_words(self):
        self.text = self.text.split(' ')

    def remove_stopwords(self):
        self.text = [word.lower() for word in self.text if word not in stopwords.words('english')]

    # https://www.machinelearningplus.com/nlp/lemmatization-examples-python/
    def get_wordnet_pos(self, word):
        """Map POS tag to first character lemmatize() accepts"""
        tag = nltk.pos_tag([word])[0][1][0].upper()
        tag_dict = {"J": wordnet.ADJ,
                    "N": wordnet.NOUN,
                    "V": wordnet.VERB,
                    "R": wordnet.ADV}

        return tag_dict.get(tag, wordnet.NOUN)

    def lemmatize_words(self):
        lemmatizer = nltk.wordnet.WordNetLemmatizer()
        self.text = [lemmatizer.lemmatize(word, self.get_wordnet_pos(word)) for word in self.text]

    def normalize_chemical_names(self):
        self.text = [
            examples.shortcuts_to_element[word] if word in examples.shortcuts_to_element else word
            for word
            in self.text]

    def list_to_str(self):
        self.text = " ".join(self.text)


CORPUS_FOLDER = Path("../corpus")

if __name__ == '__main__':
    for pdf_path in tqdm(CORPUS_FOLDER.iterdir()):
        if pdf_path.name.endswith(".pdf"):
            try:
                x = Document(str(pdf_path))
                x.remove_whitespace_before_and_after_numbers()
                #print(len(x.text))
                x.remove_line_break_dash()
                #print(len(x.text))
                x.replace_format_chars()
                #print(len(x.text))
                x.remove_non_ascii()
                #print(len(x.text))
                x.remove_punctuation()
                #print(len(x.text))
                x.remove_extra_white_spaces()
                #print(len(x.text))
                x.tokenize_words()
                #print(len(x.text))
                x.remove_stopwords()
                #print(len(x.text))
                # x.lemmatize_words()
                #x.normalize_chemical_names()
                #print(len(x.text))
                x.list_to_str()
                x.save_text()
            except Exception as e:
                print(e)
