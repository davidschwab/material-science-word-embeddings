# Material Word Embedding - Preprocessing 

# Scipdf - Installation

Supported python version: 3.10

Source: https://github.com/titipata/scipdf_parser


```
source env/bin/activate (For Windows: env\Scripts\activate)

pip install git+https://github.com/titipata/scipdf_parser

python -m spacy download en_core_web_sm
```

# Execution 

Start the Grobid Server to run scipdf_to_text
```
bash serve_grobid.sh
```

Run scipdf_to_text