import nltk

from create_wiki_compounds import get_wiki_compounds
import pandas as pd
import string
from pathlib import Path
import re
from tqdm import tqdm
from collections import Counter
from nltk.tokenize import word_tokenize
nltk.download('punkt')
import csv
import json
import numpy as np


class Document:
    text = []
    output_file_type: str = ".txt"
    output_file_path: str

    def __init__(self, file_path: str, most_common_formulas: list, most_common_elements: list,
                 most_common_compound_endings: list):
        self.mcce = most_common_compound_endings
        self.mcelements = most_common_elements
        self.mcformulas = most_common_formulas
        self.elements = []
        self.formulas = []
        self.regex_formulas = []
        with open(file_path) as f:
            self.text = f.read()
        self.text = word_tokenize(self.text)
        #if txt_path.name.endswith('Co2FeSi Based Magnetic Tunnel Junctions With BaO Barrier.txt'):
        #    print(self.text)

    def check_for_compounds_endings(self):
        for word in self.text:
            if word.endswith(tuple(self.mcce)):
                self.elements.append(word)
        return self.elements

    def check_for_compounds_elements(self):
        for word in self.text:
            if word in self.mcelements:
                self.elements.append(word)
        return self.elements

    def check_for_compounds_formulas(self):
        for word in self.text:
            if word in self.mcformulas:
                self.formulas.append(word)
        return self.formulas

    def check_for_variable_formulas(self):
        for word in self.text:
            #x = re.search("(\\w*([A-Z][a-z]?)(\d+(?:(?:[\.|\,])\d+(?:\%)?)?)([A-Z][a-z]?)\\w*)", word, re.IGNORECASE)
            x = re.search("(?:[A-Z]+)(?:\d+(?:(?:[\.|\,])\d+(?:\%)?)?)(?:[A-Z]+)(?:\d+(?:(?:[\.|\,])\d+(?:\%)?)?)*(?:[A-Z]*)(?:\d+(?:(?:[\.|\,])\d+(?:\%)?)?)*", word, re.IGNORECASE)
            if x:
                self.regex_formulas.append(word)
        return self.regex_formulas





CORPUS_FOLDER = Path("./corpus")

if __name__ == '__main__':
    most_common_formulas, most_common_elements, most_common_compound_endings = get_wiki_compounds()
    # remove elements that are only one letter
    most_common_elements = most_common_elements[most_common_elements['compound_element'].apply(lambda x: len(x) > 1)]

    elements_ = pd.DataFrame(columns=['element'])#, 'abbrev', 'files_found', 'count', 'compounds_cooccurence'])
    formulas_ = pd.DataFrame(columns=['formula'])
    regex_formulas_ = pd.DataFrame(columns=['regex_formula'])

    for txt_path in tqdm(CORPUS_FOLDER.iterdir()):
        if txt_path.name.endswith(".txt"):
            try:
                x = Document(str(txt_path), list(most_common_formulas.compound_formula), list(most_common_elements.compound_element), list(most_common_compound_endings.compound_ending))
                # check if listed element is in text
                elements = x.check_for_compounds_elements()
                formulas = x.check_for_compounds_formulas()
                regex_formulas = x.check_for_variable_formulas()
                elements_counts = Counter(elements)
                formulas_counts = Counter(formulas)
                regex_formulas_counts = Counter(regex_formulas)
                # create df with element and frequency from Counter
                #df = pd.DataFrame.from_dict(elements_counts, orient='index').reset_index()
                #df = df.rename(columns={"index": "element", 0: "count_per_file"})
                df1 = pd.DataFrame.from_dict(elements_counts, orient='index').reset_index()
                df1 = df1.rename(columns={"index": "element", 0:"count_per_file"})
                df2 = pd.DataFrame.from_dict(formulas_counts, orient='index').reset_index()
                df2 = df2.rename(columns={"index": "formula", 0: "count_per_file"})
                df3 = pd.DataFrame.from_dict(formulas_counts, orient='index').reset_index()
                df3 = df3.rename(columns={"index": "regex_formula", 0: "count_per_file"})
                #df = df.rename(columns={"index": "element", 0: "count_per_file"})
                df1['file'] = txt_path.name
                df2['file'] = txt_path.name
                df3['file'] = txt_path.name
                elements_ = pd.concat([elements_, df1])
                formulas_ = pd.concat([formulas_, df2])
                regex_formulas_ = pd.concat([regex_formulas_, df3])
                result = elements_.groupby('element', as_index=False).agg(list)
                result2 = formulas_.groupby('formula', as_index=False).agg(list)
                result3 = regex_formulas_.groupby('regex_formula', as_index=False).agg(list)
                #endings = x.check_for_compounds_endings()
            except Exception as e:
                print(e)
    # calculate total count of element
    result.to_csv("elements_result.csv", index=False)
    result2.to_csv("formulas_result.csv", index=False)
    result3.to_csv("regex_formulas_result.csv", index=False)
    result['total_count'] = result['count_per_file'].apply(sum)
    result2['total_count'] = result2['count_per_file'].apply(sum)
    result = result.sort_values('total_count')
    result2 = result2.sort_values('total_count')
    result.to_csv("compounds_found.csv", index=False)
    result2.to_csv("formulas_compounds_found.csv", index=False)
    co_occ = result.drop('count_per_file', axis=1)
    co_occ = co_occ.drop('total_count', axis=1)
    # create dict for co-occurence matrix
    co_occ_dict = dict(zip(co_occ.element, co_occ.file))
    # create co-occurence matrix
    values = sorted(set(e for v in co_occ_dict.values() for e in v))
    co_occ_matrix = {k: [1 if value in v else 0 for value in values] for k, v in co_occ_dict.items()}
    with open('co_occurence.csv', 'w') as f:
        writer = csv.writer(f)
        writer.writerow(values)
        for k, v in co_occ_matrix.items():
            writer.writerow([k, v])
    list = result.element

    out = {}
    # keep the keys in sorted order
    sorted_keys = sorted(co_occ_matrix)

    for i in range(len(sorted_keys) - 1):
        out[sorted_keys[i]] = []
        for j in range(len(sorted_keys) - 1 ):
            co_occ = sum([a * b for a, b in zip(co_occ_matrix[sorted_keys[i]], co_occ_matrix[sorted_keys[j]])])
            if co_occ > 0:
                out[sorted_keys[i]].append([sorted_keys[j]])

    def min_distance(text, w1, w2):
        index1 = None
        index2 = None
        distance = 1000000
        for idx, word in enumerate(text[0].split(" ")):
            if word == w1:
                if index2 is not None:
                    distance = min(distance, abs(idx - index2) - 1)
                index1 = idx
            if word == w2:
                if index1 is not None:
                    distance = min(distance, abs(idx - index1) - 1)
                index2 = idx
        if index1 is not None and index2 is not None:
            return distance
        return -1

    for k, v in out.items():
        for i in range(len(v)):
            if set(co_occ_dict[k]).intersection(co_occ_dict[v[i][0]]):
                iterator = iter(set(co_occ_dict[k]).intersection(co_occ_dict[v[i][0]]))
                v[i].append([])
                for r in range(len(set(co_occ_dict[k]).intersection(co_occ_dict[v[i][0]]))):
                    matching_file = next(iterator, None)
                    with open('./corpus/' + str(matching_file)) as f:
                        text = f.readlines()
                        min_dist = min_distance(text, k, v[i][0])
                        v[i][1].append(min_dist)

    for k, v in out.items():
        to_remove = []
        for i in range(len(v)):
            if v[i][1][0] == -1:
                to_remove.append(i)
        for index in sorted(to_remove, reverse=True):
            del v[index]

    def Convert(a):
        it = iter(a)
        dct = dict(zip(it, it))
        return dct

    for k, v in out.items():
        for i in range(len(v)):
            #v[i] = dict.fromkeys(v[i], v[i][1])
            v[i][1] = sorted(v[i][1])
            v[i] = Convert(v[i])

    with open('elements_co_occ.csv', 'w') as f:  # You will need 'wb' mode in Python 2.x
        w = csv.DictWriter(f, out.keys())
        w.writeheader()
        w.writerow(out)

    with open('frontend/public/co_occurences.json', 'w') as f:
        json.dump(out, f)
    result = json.dumps(out)




