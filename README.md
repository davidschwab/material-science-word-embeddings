# Material word embeddings

# Installation

Supported python version: 3.10

We recommend using a virtual environment especially to avoid dependency problems.

Create virtual environment:

```
python -m pip install --user virtualenv

python -m venv env

source env/bin/activate (For Windows: env\Scripts\activate)
```

Install requirements:

```
pip install -r requirements.txt
```