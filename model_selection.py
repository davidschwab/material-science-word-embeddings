import hashlib
import pickle
import warnings
from dataclasses import dataclass
from pathlib import Path

import numpy as np
from gensim.models import Word2Vec
from sklearn.manifold import TSNE

from project_config import SAVED_MODEL_PATH, REDUCED_MODEL_PATH


@dataclass
class ReducedModel:
    x: list
    y: list
    z: list
    labels: list[str]


class ModelFolderNotFoundException(Exception):
    pass


class ModelNotAvailableException(Exception):
    pass


class ModelSelector:
    model_names: [str]
    model_file_names: [str]
    folder_path: Path
    model_file_extension: str
    models: dict[str, Word2Vec]
    reduced_models: dict[str, ReducedModel]

    def __init__(self, folder_path: Path = SAVED_MODEL_PATH, model_file_extension: str = "model"):
        self.folder_path = folder_path
        self.model_file_extension = model_file_extension
        try:
            list(self.folder_path.iterdir())
        except FileNotFoundError:
            raise ModelFolderNotFoundException
        self.model_file_names = [str(model) for model in self.folder_path.iterdir()
                                 if model.name.endswith("." + self.model_file_extension)]
        self.model_names = [model.replace("." + self.model_file_extension, "").split("/")[-1] for model in
                            self.model_file_names]
        self.models = dict(zip(self.model_names, [Word2Vec.load(model_path) for model_path in self.model_file_names]))
        print("reducing models...")
        self.reduced_models = {model_name: get_reduced_model(model) for model_name, model in self.models.items()}
        print("done")

    def get_model_path_from_name(self, name: str):
        file_name = f"{name}.{self.model_file_extension}"
        if file_name in self.model_names:
            model_path = Path(f"{self.folder_path.name}/{file_name}")
            return model_path
        raise ModelNotAvailableException

    def get_model_from_name(self, name: str):
        return self.models[name]

    def get_reduced_model_from_name(self, name: str):
        return self.reduced_models[name]

    def get_vectors_from_label(self, labels: list[str], model_name: str) -> ReducedModel:
        reduced_model = self.get_reduced_model_from_name(model_name)
        filt_x, filt_y, filt_z, filt_labels = [], [], [], []

        for label in labels:
            if label in reduced_model.labels:
                i = labels.index(label)
                filt_x.append(float(reduced_model.x[i]))
                filt_y.append(float(reduced_model.y[i]))
                filt_z.append(float(reduced_model.z[i]))
                filt_labels.append(label)

        return ReducedModel(filt_x, filt_y, filt_z, filt_labels)

    def is_word_in_model(self, word: str, model_name: str) -> bool:
        model = self.get_model_from_name(model_name)
        try:
            model.wv.get_vector(word)
            return True
        except KeyError:
            return False


def reduce_model(model: Word2Vec, final_dimension_num: int = 3) -> ReducedModel:
    vectors = np.asarray(model.wv.vectors)
    labels = np.asarray(model.wv.index_to_key)
    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        tsne = TSNE(n_components=final_dimension_num, random_state=0, init='pca', learning_rate='auto')
        vectors = tsne.fit_transform(vectors)

    x, y, z = [], [], []
    for v in vectors:
        x.append(v[0])
        y.append(v[1])
        z.append(v[2])

    return ReducedModel(x, y, z, list(labels))


def get_reduced_model(model: Word2Vec, final_dimension_num: int = 3, use_cache=True) -> ReducedModel:
    hashed_model = hashlib.sha224(model.wv.vectors).hexdigest()
    cached_model = hashed_model + ".pickle"
    if use_cache:
        REDUCED_MODEL_PATH.mkdir(exist_ok=True)
        if cached_model in [file.name for file in REDUCED_MODEL_PATH.iterdir()]:
            print("loading cached model " + cached_model)
            with open(f"{REDUCED_MODEL_PATH.name}/{cached_model}", 'rb') as f:
                data = pickle.load(f)
            return data
        else:
            print(f"model {hashed_model} not found. Generating new embeddings. This will take some time.")
    red_model = reduce_model(model, final_dimension_num)
    if use_cache:
        print(f"Done. Saving Model {hashed_model}")
        with open(f"{REDUCED_MODEL_PATH.name}/{cached_model}", 'wb') as f:
            pickle.dump(red_model, f, pickle.HIGHEST_PROTOCOL)
    return red_model
