from gensim.models.keyedvectors import KeyedVectors


def get_similar_to_relation(wv: KeyedVectors, first: str, second_vec: str,
                            third: str, top_n: int) -> [int, str]:
    try:
        first_vec = wv.get_vector(first)
        second_vec = wv.get_vector(second_vec)
        third_vec = wv.get_vector(third)

        relation_vector = first_vec - second_vec + third_vec
        similar_by_vector = wv.similar_by_vector(relation_vector,
                                                 topn=top_n)
        return similar_by_vector
    except KeyError:
        return []
