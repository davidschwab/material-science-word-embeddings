import { RouteRecordRaw } from 'vue-router';

const routes: RouteRecordRaw[] = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/TopSimilarity.vue') },
      { path: 'plot', component: () => import('pages/PlotView.vue') },
      { path: 'relation', component: () => import('pages/Relation.vue') },
      { path: 'settings', component: () => import('pages/SettingsPage.vue') },
      ],
  }
];

export default routes;
