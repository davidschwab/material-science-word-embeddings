enum ModelEvaluator {
  TopSimilarity,
  Relation,
  Plot
}

function modelEvaluatorFromPath(path: string): ModelEvaluator|null {
  switch (path) {
    case '/':
      return ModelEvaluator.TopSimilarity
    case '/relation':
      return ModelEvaluator.Relation
    case '/plot':
      return ModelEvaluator.Plot
    default:
      return null
  }
}


export {ModelEvaluator, modelEvaluatorFromPath}

