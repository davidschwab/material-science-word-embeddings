import { boot } from 'quasar/wrappers';
import { VueWindowSizePlugin } from 'vue-window-size/option-api';

// more info on params: https://v2.quasar.dev/quasar-cli/boot-files
export default boot(({ app }) => {
  app.use(VueWindowSizePlugin);
});
