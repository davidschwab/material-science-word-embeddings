import {defineStore} from 'pinia';
import {ref} from 'vue';
import {wordListPeriodicTable, listEntry, compounds, cooccurences} from 'src/exampleData/wordList';


export const useWordManagementStore = defineStore('wordManagement', () => {
  const words = ref<listEntry[]>([wordListPeriodicTable, compounds, cooccurences]);
  const useLinebreaks = ref(true);
  const listInUse = ref('Periodic Element Table');
  const getIndexForList = (list: string) => {
    for (let i = 0; i < words.value.length; i++) {
      if (words.value[i].name === list) {
        return i
      }
    }
    console.log('couldnt find entry: ' + list)
    return -1
  }

  const deleteList = (name: string) => words.value = words.value.filter((entry) => entry.name !== name)

  const flushWords = (list: string) => words.value[getIndexForList(list)].content = []
  const addWord = (word: string, list: string) => words.value[getIndexForList(list)].content.push(word)

  function getTextForInputField(list: string): string {
    return words.value[getIndexForList(list)].content.join(useLinebreaks.value ? '\n' : ' ')
  }

  const getLists = (): string[] => words.value.map((entry) => entry.name)
  const addList = (name: string) => words.value.push({name: name, content: []})
  const updateList = (name: string, value: string[]) => words.value[getIndexForList(name)].content = value
  const getList = (name: string) => words.value[getIndexForList(name)].content
  const getCurrentList = () => getList(listInUse.value)


  return {
    words,
    listInUse,
    getTextForInputField,
    flushWords,
    addWord,
    getLists,
    addList,
    updateList,
    deleteList,
    getList,
    getCurrentList
  }
}, {
  persist: true
});
