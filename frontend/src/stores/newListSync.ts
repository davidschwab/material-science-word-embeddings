import { defineStore } from 'pinia';
import {ref} from 'vue';

export const useCounterStore = defineStore('newList', () => {
  const showDialog = ref(false);

  return {showDialog}
});
