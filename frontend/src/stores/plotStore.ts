import {defineStore} from 'pinia';
import {ref} from 'vue';

export const usePlotStore = defineStore('plot', () => {
  const plotHeight = ref(0);
  const setPlotHeight = (n: number | undefined) => plotHeight.value = typeof n === 'number' ? n : 0;
  const getPlotHeight = () => plotHeight;

  return {plotHeight, setPlotHeight, getPlotHeight}
});
