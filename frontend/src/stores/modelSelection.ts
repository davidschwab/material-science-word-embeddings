import {defineStore} from 'pinia';
import {ref} from 'vue';
import {api} from 'boot/axios';
import {ModelEvaluator} from 'components/model';

export const useModelSelectionStore = defineStore('modelSelection', () => {
  const availableModels = ref<string[]>([]);
  const selectedModels = ref<(string | null)[]>([null, null, null]) // only access with ModelSelector

  function setPreferredModel(modelName: string|null) {
    const newSelection = modelName != null && availableModels.value.includes(modelName)
      ? modelName : null;
    selectedModels.value = [newSelection, newSelection, newSelection];
  }

  function setAvailableModels(modelList: string[] | null) {
    if (modelList != null) {
      availableModels.value = modelList;
    }
  }
  selectedModels.value = ['pretrained_embeddings', 'pretrained_embeddings', 'pretrained_embeddings']

  const getModel = (eva: ModelEvaluator) => selectedModels.value[eva]

  const setModel = (modelName: string, eva: ModelEvaluator) =>
    selectedModels.value[eva] = availableModels.value.includes(modelName) ? modelName : null


  return {setPreferredModel, availableModels, setAvailableModels, setModel, selectedModels, getModel}
}, {
  persist: {
    beforeRestore: () => {
      const store = useModelSelectionStore();
      api.get('models').then((resp) => store.setAvailableModels(resp.data))
    }
  }
});
