"""
Diese Datei ist dazu da, damit das Frontend im Production Modus vom Server
geliefert werden kann und alle anfragen, die nicht direkt an die api gehen,
zu diesem weitergeleitet werden.
"""

from fastapi import APIRouter, Request
from fastapi.templating import Jinja2Templates

import mimetypes

fe = APIRouter()

templates = Jinja2Templates(directory="frontend/dist/spa")
mimetypes.init()


@fe.api_route("/{path_name:path}", methods=["GET"])
async def catch_all(request: Request):
    mimetypes.add_type('application/javascript', '.js')
    mimetypes.add_type('text/css', '.css')
    mimetypes.add_type('image/svg+xml', '.svg')
    return templates.TemplateResponse("index.html", {"request": request},
                                      media_type='text/html')
