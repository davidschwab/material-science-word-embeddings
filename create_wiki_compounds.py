import pandas as pd

def get_wiki_compounds():
    compounds = pd.read_csv('./compounds/wiki_compounds_lc.csv')
    #print(compounds.head())

    most_common_compound_formulas = compounds.chemical_formula.str.split(expand=True).stack().value_counts().reset_index()
    most_common_compound_formulas.columns = ['compound_formula', 'frequency']

    most_common_compound_elements = compounds.synonyms.str.split(expand=True).stack().value_counts().reset_index()
    most_common_compound_elements.columns = ['compound_element', 'frequency']
    #print(most_common_compound_elements.head())


    most_common_compound_endings = compounds.synonyms.str[-3:].value_counts().reset_index()
    most_common_compound_endings.columns = ['compound_ending', 'frequency']
    #print(set(most_common_compound_endings.compound_ending))

    # We set the minimum of occurences to 10
    most_common_compound_endings = most_common_compound_endings[most_common_compound_endings['frequency'] >= 10]
    #print(most_common_compound_endings.head(30))

    return most_common_compound_formulas, most_common_compound_elements, most_common_compound_endings