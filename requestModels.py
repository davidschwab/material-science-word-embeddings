from pydantic import BaseModel


class DisplayVectorsRequest(BaseModel):
    model: str
    vectors: list[str]


class RelationRequest(BaseModel):
    model: str
    first: str
    second: str
    third: str
    top_n: int = 10
